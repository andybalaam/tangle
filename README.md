# Tangle

A multi-player online Tron game in Rust.

## Prerequisites

* Install [Rust](https://www.rust-lang.org/)
* `rustup component add rustfmt`

## Build and run

To build and run tests:

```bash
make
```

To run the server:

```bash
cargo run
```

## License

Copyright 2019 Andy Balaam.

Free Software released under the AGPLv3.  See [LICENSE](LICENSE) for details.
