#!/bin/bash

set -e
set -u

HOST="http://127.0.0.1:8088"
NAME1="My Player 1"
NAME2="My Player 2"

function join() {
    curl \
    --silent \
    "${HOST}/join" \
    --header 'Content-Type: application/json' \
    --data '{"name": "'"$1"'"}'
}

function is_starting() {
    if [[ "$1" == "Running" ]]; then
        return 1
    fi
    jq -e .StartingIn <<<$1 >/dev/null
}

function is_running() {
    [[ "$1" == "Running" ]]
}

echo -n "Joining ... "

JOIN1=$(join "${NAME1}")
JOIN2=$(join "${NAME2}")

GAMEID=$(jq -r .gameid <<<${JOIN1})

echo "done. gameid=${GAMEID}"

SECRET1=$(jq -r .secret <<<${JOIN1})
SECRET2=$(jq -r .secret <<<${JOIN2})

#echo "GAMEID=${GAMEID}"
#echo "SECRET=${SECRET}"

STATUS='{"StartingIn": 5001}'

echo -n "Waiting to start "
while is_starting "${STATUS}"; do
    sleep 1
    echo -n .
    GAME=$(curl --silent "${HOST}/games/${GAMEID}")
    STATUS=$(jq -r .status <<<${GAME})
done
echo " done"

while is_running "${STATUS}" >/dev/null; do
    GRID=$(jq -r .grid <<<${GAME})
    T=$(jq -r .t <<<${GAME})
    STATUS=$(jq -r .status <<<${GAME})
    GAME=$(curl --silent "${HOST}/games/${GAMEID}")
    echo "T=${T}"
    sleep 0.05s
done

echo "STATUS=${STATUS}"
