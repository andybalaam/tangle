use actix_web::dev::AsyncResult;
use actix_web::error::*;
use actix_web::HttpResponse;
use std::fmt::{Display, Formatter, Result};

pub fn int_error(message: &'static str) -> HttpResponse {
    HttpResponse::InternalServerError().reason(message).finish()
}

pub fn async_int_error(message: &'static str) -> AsyncResult<HttpResponse> {
    int_error(message).into()
}

#[derive(Debug, PartialEq)]
pub struct GameNotFound {
    gameid: String,
}

impl GameNotFound {
    pub fn for_gameid(gameid: &str) -> Error {
        ErrorNotFound(GameNotFound {
            gameid: String::from(gameid),
        })
    }
}

impl Display for GameNotFound {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "Game ID not found: '{}'", self.gameid)
    }
}

#[derive(Debug, PartialEq)]
pub struct PlayerNotFound {
    name: String,
}

impl PlayerNotFound {
    pub fn for_name(name: &str) -> Error {
        ErrorNotFound(PlayerNotFound {
            name: String::from(name),
        })
    }
}

impl Display for PlayerNotFound {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(
            f,
            "Player with name '{}' and the supplied secret not found.",
            self.name
        )
    }
}

#[derive(Debug, PartialEq)]
pub struct InvalidDirection {
    dir: [i8; 2],
}

impl InvalidDirection {
    pub fn new(dir: [i8; 2]) -> Error {
        ErrorBadRequest(InvalidDirection { dir })
    }
}

impl Display for InvalidDirection {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(
            f,
            "Invalid direction '{:?}' - should be [1,0], [-1,0], [0,1] or [0,-1]",
            self.dir
        )
    }
}
