use crate::app::State;
use crate::errors::int_error;
use crate::messages::games::GamesRequest;
use actix_web::{AsyncResponder, Error, HttpRequest, HttpResponse};
use tokio::prelude::Future;

pub fn games(
    req: &HttpRequest<State>,
) -> Box<Future<Item = HttpResponse, Error = Error>> {
    req.state()
        .game_list_addr
        .send(GamesRequest {})
        .from_err()
        .and_then(|res| match res {
            Ok(ok_res) => Ok(HttpResponse::Ok().json(ok_res)),
            Err(_) => Ok(int_error("GamesRequest failure.")),
        })
        .responder()
}
