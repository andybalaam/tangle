use crate::app::State;
use crate::errors::int_error;
use crate::messages::join::JoinRequest;
use actix_web::{
    AsyncResponder, Error, HttpMessage, HttpRequest, HttpResponse,
};
use tokio::prelude::Future;

pub fn join(
    req: &HttpRequest<State>,
) -> Box<Future<Item = HttpResponse, Error = Error>> {
    let addr = req.state().game_list_addr.clone();
    req.json::<JoinRequest>()
        .from_err()
        .and_then(move |join_req| {
            addr.send(join_req).from_err().and_then(|res| match res {
                Ok(ok_res) => Ok(HttpResponse::Ok().json(ok_res)),
                Err(_) => Ok(int_error("JoinRequest failure.")),
            })
        })
        .responder()
}
