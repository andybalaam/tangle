use crate::app::State;
use crate::errors::async_int_error;
use crate::errors::int_error;
use crate::messages::view::ViewRequest;
use actix_web::{
    AsyncResponder, Error, FromRequest, HttpRequest, HttpResponse, Path,
};
use tokio::prelude::Future;

pub fn view(
    req: &HttpRequest<State>,
) -> Box<Future<Item = HttpResponse, Error = Error>> {
    match Path::extract(req) {
        Ok(path) => view_path(req, path),
        Err(_) => Box::new(async_int_error("View path conversion failure.")),
    }
}

#[derive(Debug, Deserialize)]
struct ViewPath {
    gameid: String,
}

fn view_path(
    req: &HttpRequest<State>,
    path: Path<ViewPath>,
) -> Box<Future<Item = HttpResponse, Error = Error>> {
    req.state()
        .game_list_addr
        .send(ViewRequest {
            gameid: path.gameid.clone(),
        })
        .from_err()
        .and_then(|res| match res {
            Ok(ok_res) => Ok(HttpResponse::Ok().json(ok_res)),
            Err(_) => Ok(int_error("ViewRequest failure.")),
        })
        .responder()
}
