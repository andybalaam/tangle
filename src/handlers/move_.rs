use crate::app::State;
use crate::errors::async_int_error;
use crate::errors::int_error;
use crate::messages::move_::MoveRequest;
use actix_web::{
    AsyncResponder, Error, FromRequest, HttpMessage, HttpRequest, HttpResponse,
    Path,
};
use tokio::prelude::Future;

pub fn move_(
    req: &HttpRequest<State>,
) -> Box<Future<Item = HttpResponse, Error = Error>> {
    match Path::extract(req) {
        Ok(path) => move_path(req, path),
        Err(_) => Box::new(async_int_error("Move path conversion failure.")),
    }
}

#[derive(Debug, Deserialize)]
struct MovePath {
    gameid: String,
}

#[derive(Debug, Deserialize)]
struct MoveRequestBody {
    player_name: String,
    player_secret: String,
    new_dir: [i8; 2],
}

fn move_path(
    req: &HttpRequest<State>,
    path: Path<MovePath>,
) -> Box<Future<Item = HttpResponse, Error = Error>> {
    let game_list_addr = req.state().game_list_addr.clone();
    req.json()
        .from_err()
        .and_then(move |body: MoveRequestBody| {
            game_list_addr
                .send(MoveRequest {
                    gameid: path.gameid.clone(),
                    player_name: body.player_name,
                    player_secret: body.player_secret,
                    new_dir: body.new_dir,
                })
                .from_err()
                .and_then(|res| match res {
                    Ok(ok_res) => Ok(HttpResponse::Ok().json(ok_res)),
                    Err(_) => Ok(int_error("MoveRequest failure.")),
                })
        })
        .responder()
}
