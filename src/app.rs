use crate::game_list::GameList;
use crate::handlers::games;
use crate::handlers::join;
use crate::handlers::move_;
use crate::handlers::view;
use actix::prelude::*;
use actix_web::middleware::cors::Cors;
use actix_web::App;

pub struct State {
    pub game_list_addr: Addr<GameList>,
}

pub fn create(game_list_addr: Addr<GameList>) -> App<State> {
    App::with_state(State { game_list_addr }).configure(|app| {
        Cors::for_app(app)
            .allowed_origin("http://localhost:8000")
            .resource(r"/games", |r| r.get().a(games::games))
            .resource(r"/games/{gameid}", |r| {
                r.post().a(move_::move_);
                r.get().a(view::view);
            })
            .resource(r"/join", |r| r.post().a(join::join))
            .register()
    })
}
