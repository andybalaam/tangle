extern crate actix;
extern crate actix_web;
#[macro_use]
extern crate serde;
use actix::prelude::*;
use actix_web::server;

pub mod app;
mod errors;
pub mod game;
pub mod game_list;
mod handlers;
pub mod messages;
mod sec;

use crate::game_list::GameList;

pub fn run() {
    let game_list = GameList::new();
    let game_list_addr = game_list.start();

    let host = "127.0.0.1:8088";
    println!("Running on {}...", host);

    server::new(move || app::create(game_list_addr.clone()))
        .bind(host)
        .unwrap()
        .run();
}
