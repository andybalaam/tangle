use ring::digest;
use ring::rand::SecureRandom;

pub fn new_secret(rand: &SecureRandom) -> String {
    let mut x = [0 as u8; 32];
    rand.fill(&mut x).unwrap();

    digest::digest(&digest::SHA256, &x)
        .as_ref()
        .iter()
        .map(|i| format!("{:X}", i))
        .collect::<Vec<_>>()
        .join("")
}
