use std::collections::HashMap;
use std::ops::{Add, AddAssign, Sub};

#[derive(Clone, Debug)]
pub struct Game {
    grid: Grid,
    pub players: Vec<Player>,
    pub time_step: u32,
}

impl Game {
    pub fn with_player(player: Player) -> Game {
        let height: usize = 100;
        let width: usize = 100;
        let mut game = Game {
            grid: Grid {
                height: height as i32,
                width: width as i32,
                values: Vec::with_capacity(height * width),
            },
            players: vec![player],
            time_step: 0,
        };
        for _ in 0..(height * width) {
            game.grid.values.push(1);
        }
        game
    }

    pub fn add_player(&mut self, player: Player) -> usize {
        self.players.push(player);
        self.players.len() - 1
    }

    pub fn find_player(
        &mut self,
        name: &str,
        secret: &str,
    ) -> Option<&mut Player> {
        self.players
            .iter_mut()
            .find(|p| p.name == name && p.secret == secret)
    }

    /// Steps forward one time step, and returns true if two or
    /// more players are alive afterwards.
    pub fn step(&mut self) -> bool {
        fn move_players(players: &mut Vec<Player>) -> HashMap<Position, u8> {
            let mut position_counts = HashMap::new();
            for player in players.iter_mut().filter(|p| p.alive) {
                // Move our head to the new position
                player.pos += player.dir;
                // Remember how many heads are here, so we can check
                // for head-to-head collisions later.
                let count = position_counts.entry(player.pos).or_insert(0);
                *count += 1;
            }
            position_counts
        }

        fn head_to_head(
            players: &mut Vec<Player>,
            grid: &mut Grid,
            position_counts: HashMap<Position, u8>,
        ) {
            for (i, player) in
                players.iter_mut().filter(|p| p.alive).enumerate()
            {
                let x = player.pos.x;
                let y = player.pos.y;
                if position_counts[&player.pos] > 1 {
                    // Two heads in the same place.  They both die!
                    player.alive = false;
                    // Fill in grid here.  Both are dead, so won't be
                    // filled in in collisions_or_fill_grid.
                    grid.set(x, y, Square::Colour(i as u8));
                }
            }
        }

        fn collide_and_update_grid(players: &mut Vec<Player>, grid: &mut Grid) {
            for (i, player) in
                players.iter_mut().filter(|p| p.alive).enumerate()
            {
                let x = player.pos.x;
                let y = player.pos.y;
                match grid.get(x, y) {
                    Square::Colour(_) | Square::Outside => {
                        player.alive = false;
                    }
                    Square::Empty => {
                        grid.set(x, y, Square::Colour(i as u8));
                    }
                }
            }
        }

        let position_counts = move_players(&mut self.players);
        head_to_head(&mut self.players, &mut self.grid, position_counts);
        collide_and_update_grid(&mut self.players, &mut self.grid);
        self.time_step += 1;
        self.players.iter().filter(|p| p.alive).count() >= 2
    }

    pub fn grid_as_lines(&self) -> Vec<String> {
        fn colour_to_char(base: char, colour: u8) -> char {
            (base as u8 + colour as u8) as char
        }

        fn char_at(game: &Game, x: i32, y: i32) -> char {
            for (i, player) in game.players.iter().enumerate() {
                if (player.alive && player.pos == Position { x, y }) {
                    return colour_to_char('A', i as u8);
                }
            }
            match game.grid.get(x, y) {
                Square::Outside => ' ',
                Square::Empty => '.',
                Square::Colour(c) => colour_to_char('a', c),
            }
        }

        let mut ret = Vec::with_capacity(self.grid.height as usize);
        for y in 0..self.grid.height {
            let mut line = String::with_capacity(self.grid.width as usize);
            for x in 0..self.grid.width {
                line.push(char_at(self, x, y));
            }
            ret.push(line);
        }
        ret
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Player {
    pub name: String,
    pub secret: String,
    pub pos: Position,
    pub dir: Offset,
    pub alive: bool,
}

impl Player {
    pub fn new(name: &str, secret: &str, x: i32, y: i32) -> Player {
        Player {
            name: String::from(name),
            secret: String::from(secret),
            pos: Position { x, y },
            dir: Offset { dx: 0, dy: -1 },
            alive: true,
        }
    }
}

#[derive(Clone, Debug)]
struct Grid {
    width: i32,
    height: i32,
    values: Vec<u8>,
}

impl Grid {
    fn get(&self, x: i32, y: i32) -> Square {
        if x < 0 || x >= self.width || y < 0 || y >= self.height {
            Square::Outside
        } else {
            let i = (x + (y * self.width)) as usize;
            match self.values.get(i) {
                None => Square::Outside,
                Some(1) => Square::Empty,
                Some(c) => Square::Colour(c - 2),
            }
        }
    }

    fn set(&mut self, x: i32, y: i32, value: Square) {
        if x >= 0 && x < self.width && y >= 0 && y < self.height {
            self.values[(x + (y * self.width)) as usize] = match value {
                Square::Outside => {
                    panic!("Can't set a value inside the grid to Outside!")
                }
                Square::Empty => 0,
                Square::Colour(c) => c + 2,
            }
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

impl Add<Offset> for Position {
    type Output = Position;
    fn add(self, rhs: Offset) -> Position {
        Position {
            x: self.x + rhs.dx,
            y: self.y + rhs.dy,
        }
    }
}

impl AddAssign<Offset> for Position {
    fn add_assign(&mut self, other: Offset) {
        self.x += other.dx;
        self.y += other.dy;
    }
}

impl Sub<Offset> for Position {
    type Output = Position;
    fn sub(self, rhs: Offset) -> Position {
        Position {
            x: self.x - rhs.dx,
            y: self.y - rhs.dy,
        }
    }
}

impl Sub<Position> for Position {
    type Output = Offset;
    fn sub(self, rhs: Position) -> Offset {
        Offset {
            dx: self.x - rhs.x,
            dy: self.y - rhs.y,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Offset {
    pub dx: i32,
    pub dy: i32,
}

#[derive(Clone, Debug, Eq, PartialEq)]
enum Square {
    Outside,
    Empty,
    Colour(u8),
}

#[cfg(test)]
mod tests {
    use super::*;

    impl Player {
        fn from_pos(name: &str, secret: &str, x: i32, y: i32) -> Player {
            Player {
                name: String::from(name),
                secret: String::from(secret),
                pos: Position { x, y },
                dir: Offset { dx: 0, dy: 0 },
                alive: true,
            }
        }
    }

    impl Game {
        fn from_map(m: &str) -> Game {
            let mut width: Option<i32> = None;
            let mut height: i32 = 0;
            let mut values: Vec<u8> = vec![];
            let mut players: Vec<Player> = vec![];
            let mut arrows: Vec<(char, Position)> = vec![];
            for ln in trim_and_split(m) {
                let y: i32 = height;
                height += 1;
                let new_w = ln.len() as i32;
                width = map_width(width, new_w, height, m);
                let mut x: i32 = 0;
                for c in ln.chars() {
                    values.push(map_char_to_grid_num(c, height, m));
                    check_new_player(&mut players, x, y, c);
                    check_map_arrow(&mut arrows, x, y, c);
                    x += 1;
                }
            }
            apply_directions(arrows, &mut players);

            if let Some(w) = width {
                Game {
                    grid: Grid {
                        width: w,
                        height,
                        values,
                    },
                    players,
                    time_step: 0,
                }
            } else {
                panic!("No lines provided to make game map!");
            }
        }

        fn to_string(&self) -> String {
            self.grid_as_lines().join("\n")
        }
    }

    fn apply_directions(
        arrows: Vec<(char, Position)>,
        players: &mut Vec<Player>,
    ) {
        for arrow in arrows {
            let (c, pos) = arrow;
            for player in players.iter_mut() {
                let c_dir = dir_of(c);
                if player.pos == pos - c_dir {
                    player.dir = pos - player.pos
                }
            }
        }
    }

    fn dir_of(c: char) -> Offset {
        match c {
            '>' => Offset { dx: 1, dy: 0 },
            '<' => Offset { dx: -1, dy: 0 },
            '^' => Offset { dx: 0, dy: -1 },
            'v' => Offset { dx: 0, dy: 1 },
            _ => panic!("Unrecognised direction character {}", c),
        }
    }

    fn map_char_to_grid_num(c: char, height: i32, m: &str) -> u8 {
        match c {
            '.' | '<' | '>' | '^' | 'v' => 1,
            'a'...'z' => (c as u32 - ('a' as u32) + 2) as u8,
            'A'...'Z' => (c as u32 - ('A' as u32) + 2) as u8,
            _ => {
                panic!(
                    "Unrecognised symbol '{}' at line {} \
                     in game map: \n\
                     {}\n",
                    c, height, m
                );
            }
        }
    }

    fn check_new_player(players: &mut Vec<Player>, x: i32, y: i32, c: char) {
        match c {
            'A'...'Z' => {
                let index = c as usize - 'A' as usize;
                if players.len() <= index {
                    players.resize(index + 1, Player::from_pos("", "", 0, 0));
                }
                players[index] = Player::from_pos("pN", "sN", x, y);
            }
            _ => (),
        };
    }

    fn check_map_arrow(
        arrows: &mut Vec<(char, Position)>,
        x: i32,
        y: i32,
        c: char,
    ) {
        if ['<', '>', '^', 'v'].contains(&c) {
            arrows.push((c, Position { x, y }));
        }
    }

    fn trim_and_split<'a>(input: &'a str) -> impl Iterator<Item = &'a str> {
        input.split("\n").map(str::trim).filter(|s| !s.is_empty())
    }

    fn trim_lines(input: &str) -> String {
        trim_and_split(input).collect::<Vec<_>>().join("\n")
    }

    fn map_width(
        width: Option<i32>,
        new_w: i32,
        height: i32,
        m: &str,
    ) -> Option<i32> {
        match width {
            None => Some(new_w),
            Some(w) => {
                assert!(
                    w == new_w,
                    "Game map has inconsistent line widths.\n\
                     Line {} is {} characters, but\n\
                     line {} is {}.  Map:\n\
                     {}\n",
                    height - 1,
                    w,
                    height,
                    new_w,
                    m,
                );
                width
            }
        }
    }
    #[test]
    fn trim_lines_removes_leading_and_trailing_space() {
        assert_eq!(trim_lines(" # "), "#");
    }

    #[test]
    fn trim_lines_removes_leading_and_trailing_space_from_all_lines() {
        assert_eq!(trim_lines(" . \n  .. "), ".\n..");
    }

    #[test]
    fn trim_lines_removes_leading_and_trailing_empty_lines() {
        assert_eq!(trim_lines("\n . \n  .. \n"), ".\n..");
    }

    #[test]
    fn width_and_height_are_read_from_map() {
        let g = Game::from_map(
            "
            .....
            .....
            .....
            .....
        ",
        );
        assert_eq!(g.grid.width, 5);
        assert_eq!(g.grid.height, 4);
    }

    #[test]
    fn colours_are_read_from_map() {
        let g = Game::from_map(
            "
            ....
            .ab.
            .ab.
            ....
        ",
        );
        assert_eq!(g.grid.get(0, 0), Square::Empty);
        assert_eq!(g.grid.get(1, 1), Square::Colour(0));
        assert_eq!(g.grid.get(2, 2), Square::Colour(1));
    }

    #[test]
    fn squares_outside_grid_are_returned_as_outside() {
        let g = Game::from_map(
            "
            ...
            ...
            ...
        ",
        );
        assert_eq!(g.grid.get(-1, 0), Square::Outside);
        assert_eq!(g.grid.get(3, 0), Square::Outside);
        assert_eq!(g.grid.get(0, -1), Square::Outside);
        assert_eq!(g.grid.get(0, 3), Square::Outside);
        assert_eq!(g.grid.get(30, 30), Square::Outside);
        assert_eq!(g.grid.get(-30, -30), Square::Outside);
    }

    #[test]
    fn player_positions_are_read() {
        let g = Game::from_map(
            "
            .B.
            .b.
            ..A
        ",
        );
        assert_eq!(g.grid.get(1, 0), Square::Colour(1));
        assert_eq!(g.grid.get(1, 1), Square::Colour(1));
        assert_eq!(g.grid.get(2, 2), Square::Colour(0));
        assert_eq!(g.players.len(), 2);
        assert_eq!(g.players[0].pos, Position { x: 2, y: 2 });
        assert_eq!(g.players[1].pos, Position { x: 1, y: 0 });
    }

    #[test]
    fn player_directions_are_read() {
        let g = Game::from_map(
            "
            .B>^.
            .b.CD
            .<A.v
        ",
        );
        assert_eq!(g.players[0].dir, Offset { dx: -1, dy: 0 });
        assert_eq!(g.players[1].dir, Offset { dx: 1, dy: 0 });
        assert_eq!(g.players[2].dir, Offset { dx: 0, dy: -1 });
        assert_eq!(g.players[3].dir, Offset { dx: 0, dy: 1 });
    }

    fn assert_trimmed_eq(left: &str, right: &str) {
        assert_eq!(trim_lines(left), trim_lines(right))
    }

    #[test]
    fn time_step_moves_line_forward() {
        let mut g = Game::from_map(
            "
            .^...
            .A...
            .a...
            .a...
            .....
        ",
        );
        assert_eq!(g.players[0].pos, Position { x: 1, y: 1 });
        assert_eq!(g.players[0].dir, Offset { dx: 0, dy: -1 });

        g.step();

        assert_eq!(g.players[0].pos, Position { x: 1, y: 0 });
        assert_eq!(g.players[0].dir, Offset { dx: 0, dy: -1 });
        assert_trimmed_eq(
            &g.to_string(),
            "
            .A...
            .a...
            .a...
            .a...
            .....
            ",
        );
    }

    #[test]
    fn when_a_player_hits_the_edge_it_dies() {
        // Given a player next to the edge
        let mut g = Game::from_map(
            ".^...
             .A...",
        );
        g.step();
        assert!(g.players[0].alive);

        // When the player hits the edge
        g.step();

        // Then it dies
        assert!(!g.players[0].alive);
    }

    #[test]
    fn a_dead_player_does_not_move() {
        // Given a dead player
        let mut g = Game::from_map("aA>..");
        g.step();
        assert_eq!(g.players[0].pos, Position { x: 2, y: 0 });
        g.players[0].alive = false;

        // When we step
        g.step();
        g.step();
        g.step();

        // Then it has not moved
        assert_eq!(g.players[0].pos, Position { x: 2, y: 0 });
        assert_trimmed_eq(&g.to_string(), "aaa..");
    }

    macro_rules! assert_game_steps {
        ($s:expr, $( $n:expr ),+) => {
            let mut g = Game::from_map($s);
            $(
                g.step();
                assert_trimmed_eq(&g.to_string(), $n);
            )*
        };
    }

    #[test]
    fn players_leave_a_trail_as_they_move() {
        assert_game_steps!("A>...", "aA...", "aaA..", "aaaA.");
    }

    #[test]
    fn players_die_when_they_hit_another_tail() {
        assert_game_steps!("A>b..", "aAb..", "aab..", "aab..");
    }

    #[test]
    fn player_hitting_a_new_tail_dies() {
        assert_game_steps!(
            "
            ....
            .^<B
            .A..
            ....
            ",
            "
            ....
            .ABb
            .a..
            ....
            ",
            "
            .A..
            .abb
            .a..
            ....
            "
        );
    }

    #[test]
    fn two_players_at_same_place_both_die() {
        assert_game_steps!(
            "
            ....
            ..<B
            .^..
            .A..
            ",
            "
            ....
            ..Bb
            .A..
            .a..
            ",
            "
            ....
            .bbb
            .a..
            .a..
            ",
            "
            ....
            .bbb
            .a..
            .a..
            "
        );
    }

    #[test]
    fn face_on_collision() {
        assert_game_steps!(
            "
            A><B.
            C>.<D",
            "
            aABb.
            cC.Dd",
            "
            aabb.
            ccddd"
        );
    }

    #[test]
    fn step_returns_true_if_two_or_more_players_alive() {
        // Given 3 players who will die in order
        let mut g = Game::from_map(
            ".^...
             .A^..
             ..B^.
             ...C.",
        );

        // After 1 step all 3 are alive
        assert_eq!(true, g.step());

        // The first one dies but 2 are left
        assert_eq!(true, g.step());

        // Then only one is left
        assert_eq!(false, g.step());
    }
}
