use actix::prelude::*;
use actix_web::Error;
use rand;
use ring::rand::SystemRandom;
use std::time::Duration;

use crate::errors::{GameNotFound, InvalidDirection, PlayerNotFound};
use crate::game::{Game, Offset, Player};
use crate::messages::games::GameStatus;
use crate::messages::games::GamesRequest;
use crate::messages::games::GamesResponse;
use crate::messages::games::GamesResponseEntry;
use crate::messages::join::JoinRequest;
use crate::messages::join::JoinResponse;
use crate::messages::move_::MoveRequest;
use crate::messages::move_::MoveResponse;
use crate::messages::view::ViewRequest;
use crate::messages::view::ViewResponse;
use crate::sec;

#[derive(Clone, Debug)]
pub struct GameListItem {
    gameid: String,
    status: GameStatus,
    game: Game,
}

pub struct GameList {
    games: Vec<GameListItem>,
    counter: u64,
    rand: SystemRandom,
}

impl GameList {
    pub fn new() -> GameList {
        GameList {
            games: vec![],
            counter: 0,
            rand: SystemRandom::new(),
        }
    }

    fn handle_join(&mut self, req: JoinRequest) -> JoinResponse {
        let first_non_running = self
            .games
            .iter_mut()
            .filter(|i| {
                i.status != GameStatus::Running && i.game.players.len() < 6
            })
            .next();

        let secret = sec::new_secret(&self.rand);
        let x = random_x();
        let y = random_y();
        let new_player = Player::new(&req.name, &secret, x, y);
        let (item, index): (&GameListItem, usize) = match first_non_running {
            Some(i) => {
                i.status = GameStatus::StartingIn(5000);
                let index = i.game.add_player(new_player);
                (i, index)
            }
            None => {
                let i = GameListItem {
                    gameid: self.next_id(),
                    status: GameStatus::NotStarted,
                    game: Game::with_player(new_player),
                };
                self.games.push(i);
                (self.games.last().unwrap(), 0)
            }
        };
        JoinResponse {
            gameid: item.gameid.clone(),
            name: req.name.clone(),
            index,
            secret,
        }
    }

    fn find_game(&mut self, gameid: &str) -> Result<&mut GameListItem, Error> {
        // TODO: GameList should be a map, surely?
        match self.games.iter_mut().find(|gi| gi.gameid == gameid) {
            Some(game_item) => Ok(game_item),
            None => Err(GameNotFound::for_gameid(gameid)),
        }
    }

    fn handle_move(&mut self, req: MoveRequest) -> Result<MoveResponse, Error> {
        let game_item = self.find_game(&req.gameid)?;
        let time_step = game_item.game.time_step;

        let mut player = game_item
            .game
            .find_player(&req.player_name, &req.player_secret)
            .ok_or(PlayerNotFound::for_name(&req.player_name))?;

        let new_dir: Offset = translate_dir(req.new_dir)?;
        player.dir = new_dir;

        Ok(MoveResponse {
            gameid: game_item.gameid.clone(),
            t: time_step,
            x: player.pos.x,
            y: player.pos.y,
            d: req.new_dir,
            grid: game_item.game.grid_as_lines(),
        })
    }

    fn handle_view(&mut self, req: ViewRequest) -> Result<ViewResponse, Error> {
        let game_item = self.find_game(&req.gameid)?;

        Ok(ViewResponse {
            gameid: game_item.gameid.clone(),
            status: game_item.status.clone(),
            t: game_item.game.time_step,
            grid: game_item.game.grid_as_lines(),
            players: players_view(&game_item.game.players),
        })
    }

    fn handle_games(&self) -> GamesResponse {
        GamesResponse {
            games: self.games.iter().map(entry_for_item).collect(),
        }
    }

    fn handle_time_tick(&mut self, duration: Duration) {
        let mut some_finished = false;
        for game in &mut self.games {
            match game.status {
                GameStatus::StartingIn(ms) => {
                    if duration.as_millis() >= ms {
                        game.status = GameStatus::Running;
                    } else {
                        game.status =
                            GameStatus::StartingIn(ms - duration.as_millis());
                    }
                }
                GameStatus::Finished(ms) => {
                    if duration.as_millis() >= ms {
                        game.status = GameStatus::Finished(0);
                        some_finished = true;
                    } else {
                        game.status =
                            GameStatus::Finished(ms - duration.as_millis());
                    }
                }
                _ => (),
            }
        }
        if some_finished {
            // Delete games that finished over 60s ago
            self.games = self
                .games
                .clone()
                .into_iter()
                .filter(|g| {
                    if let GameStatus::Finished(ms) = g.status {
                        ms > 0
                    } else {
                        true
                    }
                })
                .collect();
        }
    }

    fn handle_game_tick(&mut self) {
        for game in &mut self
            .games
            .iter_mut()
            .filter(|g| g.status == GameStatus::Running)
        {
            let running = game.game.step();

            if !running {
                game.status = GameStatus::Finished(60000);
            }
        }
    }

    fn next_id(&mut self) -> String {
        self.counter += 1;
        self.counter.to_string()
    }
}

fn random_x() -> i32 {
    rand::random::<u8>() as i32 % 100
}

fn random_y() -> i32 {
    50 + (rand::random::<u8>() as i32 % 50)
}

fn translate_dir(input: [i8; 2]) -> Result<Offset, Error> {
    match input {
        [1, 0] => Ok(Offset { dx: 1, dy: 0 }),
        [-1, 0] => Ok(Offset { dx: -1, dy: 0 }),
        [0, 1] => Ok(Offset { dx: 0, dy: 1 }),
        [0, -1] => Ok(Offset { dx: 0, dy: -1 }),
        _ => Err(InvalidDirection::new(input)),
    }
}

impl Actor for GameList {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let tick_dur = Duration::from_secs(1);
        ctx.run_interval(
            tick_dur.clone(),
            move |game_list: &mut GameList, _| {
                game_list.handle_time_tick(tick_dur)
            },
        );
        ctx.run_interval(
            Duration::from_millis(100),
            |game_list: &mut GameList, _| game_list.handle_game_tick(),
        );
    }
}

impl Handler<GamesRequest> for GameList {
    type Result = Result<GamesResponse, Error>;

    fn handle(
        &mut self,
        _games_request: GamesRequest,
        _: &mut Self::Context,
    ) -> Self::Result {
        Ok(self.handle_games())
    }
}

impl Handler<JoinRequest> for GameList {
    type Result = Result<JoinResponse, Error>;

    fn handle(
        &mut self,
        join_request: JoinRequest,
        _: &mut Self::Context,
    ) -> Self::Result {
        Ok(self.handle_join(join_request))
    }
}

impl Handler<MoveRequest> for GameList {
    type Result = Result<MoveResponse, Error>;

    fn handle(
        &mut self,
        msg: MoveRequest,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.handle_move(msg)
    }
}

impl Handler<ViewRequest> for GameList {
    type Result = Result<ViewResponse, Error>;

    fn handle(
        &mut self,
        msg: ViewRequest,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.handle_view(msg)
    }
}

fn entry_for_item(item: &GameListItem) -> GamesResponseEntry {
    GamesResponseEntry {
        gameid: item.gameid.clone(),
        status: item.status.clone(),
    }
}

fn players_view(
    game_players: &Vec<Player>,
) -> Vec<crate::messages::view::Player> {
    game_players
        .iter()
        .map({
            |p| crate::messages::view::Player {
                name: p.name.clone(),
                pos: crate::messages::view::Position {
                    x: p.pos.x,
                    y: p.pos.y,
                },
            }
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::{Offset, Position};

    impl JoinRequest {
        fn dummy() -> JoinRequest {
            JoinRequest {
                name: String::from("dummy name"),
            }
        }
    }

    #[test]
    fn joining_causes_a_game_to_be_created() {
        let mut lst = GameList::new();
        let res = lst.handle_join(JoinRequest::dummy());

        assert_eq!(
            lst.handle_games(),
            GamesResponse {
                games: vec![GamesResponseEntry {
                    gameid: res.gameid,
                    status: GameStatus::NotStarted
                }]
            }
        );
    }

    #[test]
    fn viewing_a_new_game_shows_grid() {
        let mut lst = GameList::new();
        let gameid = lst.handle_join(JoinRequest::dummy()).gameid;
        let res = lst
            .handle_view(ViewRequest {
                gameid: gameid.clone(),
            })
            .unwrap();

        assert_eq!(res.gameid, gameid);
        assert_eq!(res.status, GameStatus::NotStarted);
        assert_eq!(res.t, 0);
        assert_eq!(res.grid.len(), 100);
        assert_eq!(res.grid[0].len(), 100);
    }

    #[test]
    fn a_second_player_makes_the_game_start_counting_down() {
        // When two players join
        let mut lst = GameList::new();
        let res1 = lst.handle_join(JoinRequest::dummy());
        let res2 = lst.handle_join(JoinRequest::dummy());

        // Both players join the same game
        assert_eq!(res1.gameid, res2.gameid);

        // Players are given indices in order
        assert_eq!(res1.index, 0);
        assert_eq!(res2.index, 1);

        // There is only one game
        assert_eq!(
            lst.handle_games(),
            GamesResponse {
                games: vec![GamesResponseEntry {
                    gameid: res1.gameid,
                    status: GameStatus::StartingIn(5000),
                }]
            }
        );
    }

    #[test]
    fn if_enough_people_joined_first_game_another_appears() {
        // Six players means the first game is full
        let mut lst = GameList::new();
        let res1 = lst.handle_join(JoinRequest::dummy());
        for n in 2..=6 {
            let resn = lst.handle_join(JoinRequest::dummy());
            assert_eq!(res1.gameid, resn.gameid);
            assert_eq!(resn.index, n - 1);
        }

        let res7 = lst.handle_join(JoinRequest::dummy());

        // The 7th player enters a different game
        assert_ne!(res1.gameid, res7.gameid);
        // With index 0
        assert_eq!(res7.index, 0);

        // There are 2 games
        assert_eq!(
            lst.handle_games(),
            GamesResponse {
                games: vec![
                    GamesResponseEntry {
                        gameid: res1.gameid,
                        status: GameStatus::StartingIn(5000),
                    },
                    GamesResponseEntry {
                        gameid: res7.gameid,
                        status: GameStatus::NotStarted,
                    }
                ]
            }
        );
    }

    #[test]
    fn game_starts_after_five_seconds() {
        fn game0_status(lst: &GameList) -> GameStatus {
            lst.handle_games().games[0].status
        }

        let mut lst = GameList::new();
        lst.handle_join(JoinRequest::dummy());
        lst.handle_join(JoinRequest::dummy());
        assert_eq!(game0_status(&lst), GameStatus::StartingIn(5000));
        lst.handle_time_tick(Duration::from_secs(4));
        assert_eq!(game0_status(&lst), GameStatus::StartingIn(1000));
        lst.handle_time_tick(Duration::from_secs(1));
        assert_eq!(game0_status(&lst), GameStatus::Running);
    }

    #[test]
    fn finished_game_disappears_after_60_seconds() {
        // Given a finished game
        fn game0_status(lst: &GameList) -> GameStatus {
            lst.handle_games().games[0].status
        }
        let mut lst = GameList::new();
        lst.handle_join(JoinRequest::dummy());
        lst.handle_join(JoinRequest::dummy());
        lst.handle_time_tick(Duration::from_secs(5));
        lst.games[0].game.players[0].alive = false;
        lst.handle_game_tick();
        assert_eq!(game0_status(&lst), GameStatus::Finished(60000));

        // Time ticks reduce its time to live
        lst.handle_time_tick(Duration::from_secs(4));
        assert_eq!(game0_status(&lst), GameStatus::Finished(56000));

        // And after 60 secs it is deleted
        lst.handle_time_tick(Duration::from_secs(56));
        assert_eq!(lst.games.len(), 0);
    }

    #[test]
    fn game_runs_every_game_step() {
        // Given a running game
        let mut lst = GameList::new();
        lst.handle_join(JoinRequest::dummy());
        lst.handle_join(JoinRequest::dummy());
        lst.games[0].game.players[0].pos = Position { x: 10, y: 10 };
        lst.games[0].game.players[1].pos = Position { x: 20, y: 10 };
        lst.handle_time_tick(Duration::from_secs(5));
        assert_eq!(lst.games[0].status, GameStatus::Running);

        assert_eq!(lst.games[0].game.time_step, 0);

        // When we tick time, the game stepped forward
        lst.handle_game_tick();
        assert_eq!(lst.games[0].game.time_step, 1);

        lst.handle_game_tick();
        assert_eq!(lst.games[0].game.time_step, 2);
    }

    #[test]
    fn game_without_2_live_players_becomes_finished() {
        // Given a running game
        let mut lst = GameList::new();
        lst.handle_join(JoinRequest::dummy());
        lst.handle_join(JoinRequest::dummy());
        lst.handle_time_tick(Duration::from_secs(5));
        assert_eq!(lst.games[0].status, GameStatus::Running);

        // Kill a player
        lst.games[0].game.players[1].alive = false;

        lst.handle_game_tick();

        assert_eq!(lst.games[0].status, GameStatus::Finished(60000));
    }

    fn game_list_with_2_games() -> GameList {
        let mut lst = GameList::new();
        let mut game0 = Game::with_player(Player::new("g0p0", "sg0p0", 0, 0));
        let mut game1 = Game::with_player(Player::new("g1p0", "sg1p0", 0, 0));
        game0.add_player(Player::new("g0p1", "sg0p1", 0, 0));
        game1.add_player(Player::new("g1p1", "sg1p1", 0, 0));
        lst.games.push(GameListItem {
            gameid: String::from("game0"),
            status: GameStatus::Running,
            game: game0,
        });
        lst.games.push(GameListItem {
            gameid: String::from("game1"),
            status: GameStatus::Running,
            game: game1,
        });
        lst
    }

    #[test]
    fn finished_games_get_deleted() {
        // Given a running game and a finished one
        let mut lst = game_list_with_2_games();
        lst.games[1].status = GameStatus::Finished(4000);
        // When time ticks past the end of the finished game's lif
        lst.handle_time_tick(Duration::from_secs(5));

        // The finished game is gone, but not the other one
        assert_eq!(lst.games.len(), 1);
        assert_eq!(lst.games[0].gameid, "game0");
    }

    const DIR_RIGHT: [i8; 2] = [1, 0];
    const DIR_LEFT: [i8; 2] = [-1, 0];
    const DIR_DOWN: [i8; 2] = [0, 1];

    #[test]
    fn attempting_to_move_in_a_nonexistent_game_is_an_error() {
        let mut lst = game_list_with_2_games();
        let gameid = String::from("nonexistentid!!111");

        let req = MoveRequest {
            gameid: gameid.clone(),
            player_name: String::from("player1"),
            player_secret: String::from("sec1"),
            new_dir: DIR_RIGHT,
        };
        let res = lst.handle_move(req);

        assert_eq!(
            format!("{:?}", res.unwrap_err()),
            format!("{:?}", GameNotFound::for_gameid(&gameid)),
        );
    }

    #[test]
    fn attempting_to_move_a_nonexistent_player_is_an_error() {
        let mut lst = game_list_with_2_games();
        let player_name = String::from("nonexistentplayer");

        let req = MoveRequest {
            gameid: lst.games[0].gameid.clone(),
            player_name: player_name.clone(),
            player_secret: String::from("sec1"),
            new_dir: DIR_RIGHT,
        };
        let res = lst.handle_move(req);

        assert_eq!(
            format!("{:?}", res.unwrap_err()),
            format!("{:?}", PlayerNotFound::for_name(&player_name)),
        );
    }

    #[test]
    fn attempting_to_move_with_the_wrong_secret_is_an_error() {
        let mut lst = game_list_with_2_games();

        let req = MoveRequest {
            gameid: lst.games[0].gameid.clone(),
            player_name: lst.games[0].game.players[0].name.clone(),
            player_secret: String::from("nonexistent secret"),
            new_dir: DIR_RIGHT,
        };
        let res = lst.handle_move(req);

        assert_eq!(
            format!("{:?}", res.unwrap_err()),
            format!(
                "{:?}",
                PlayerNotFound::for_name(&lst.games[0].game.players[0].name)
            ),
        );
    }

    #[test]
    fn move_request_turns_the_player() {
        let mut lst = game_list_with_2_games();

        // Initially all players are facing up
        assert_eq!(lst.games[0].game.players[0].dir, Offset { dx: 0, dy: -1 });
        assert_eq!(lst.games[0].game.players[1].dir, Offset { dx: 0, dy: -1 });
        assert_eq!(lst.games[1].game.players[0].dir, Offset { dx: 0, dy: -1 });
        assert_eq!(lst.games[1].game.players[1].dir, Offset { dx: 0, dy: -1 });

        let game1 = &lst.games[1];
        let player1 = &game1.game.players[1];

        // Request to turn right
        let req = MoveRequest {
            gameid: game1.gameid.clone(),
            player_name: player1.name.clone(),
            player_secret: player1.secret.clone(),
            new_dir: DIR_RIGHT,
        };
        let res = lst.handle_move(req);

        // The response says we are facing right
        assert_eq!(res.unwrap().d, [1, 0]);

        // The relevant player is now facing right
        assert_eq!(lst.games[1].game.players[1].dir, Offset { dx: 1, dy: 0 });

        // Other players are unaffected
        assert_eq!(lst.games[0].game.players[0].dir, Offset { dx: 0, dy: -1 });
        assert_eq!(lst.games[0].game.players[1].dir, Offset { dx: 0, dy: -1 });
        assert_eq!(lst.games[1].game.players[0].dir, Offset { dx: 0, dy: -1 });
    }

    #[test]
    fn example_game_plays_out() {
        // Start a game with 2 players
        let mut lst = GameList::new();
        let player0 = lst.handle_join(JoinRequest {
            name: String::from("Alice"),
        });
        let player1 = lst.handle_join(JoinRequest {
            name: String::from("Bob"),
        });
        lst.handle_time_tick(Duration::from_secs(5));

        assert_eq!(player0.gameid, player1.gameid);
        assert_eq!(player0.name, "Alice");
        assert_eq!(player1.name, "Bob");
        assert_ne!(player0.secret, player1.secret);
        // TODO: assert players are at positions within some range

        // Reset positions and directions of players to predictable values
        lst.games[0].game.players[0].pos.x = 1;
        lst.games[0].game.players[0].pos.y = 2;
        lst.games[0].game.players[0].dir.dx = 0;
        lst.games[0].game.players[0].dir.dy = -1;
        lst.games[0].game.players[1].pos.x = 4;
        lst.games[0].game.players[1].pos.y = 2;
        lst.games[0].game.players[1].dir.dx = 0;
        lst.games[0].game.players[1].dir.dy = -1;

        let req_0_left = MoveRequest {
            gameid: player0.gameid.clone(),
            player_name: player0.name.clone(),
            player_secret: player0.secret.clone(),
            new_dir: DIR_LEFT,
        };

        let req_1_left = MoveRequest {
            gameid: player1.gameid,
            player_name: player1.name,
            player_secret: player1.secret,
            new_dir: DIR_LEFT,
        };

        // Turn player 0 left
        let res = lst.handle_move(req_0_left.clone()).unwrap();

        assert_eq!(
            "\
             ......\n\
             ......\n\
             .A..B.\n\
             ......\n\
             ......\n\
             ......\
             ",
            top_left_of(res.grid)
        );
        assert_eq!(res.t, 0);
        assert_eq!(res.x, 1);
        assert_eq!(res.y, 2);
        assert_eq!(res.d, [-1, 0]);

        lst.handle_game_tick();

        // Repeat the same move request
        let res = lst.handle_move(req_0_left.clone()).unwrap();

        // Note: at the moment, we don't leave a trail from the first
        // time step.
        assert_eq!(
            "\
             ......\n\
             ....B.\n\
             A.....\n\
             ......\n\
             ......\n\
             ......\
             ",
            top_left_of(res.grid)
        );
        assert_eq!(res.t, 1);
        assert_eq!(res.x, 0);
        assert_eq!(res.y, 2);
        assert_eq!(res.d, [-1, 0]);

        // Turn player zero downwards and player 1 left
        lst.handle_move(MoveRequest {
            gameid: player0.gameid,
            player_name: player0.name,
            player_secret: player0.secret,
            new_dir: DIR_DOWN,
        })
        .unwrap();
        lst.handle_move(req_1_left.clone()).unwrap();

        lst.handle_game_tick();

        // Turn left again, so we will die next time
        let res = lst.handle_move(req_0_left).unwrap();

        assert_eq!(
            "\
             ......\n\
             ...Bb.\n\
             a.....\n\
             A.....\n\
             ......\n\
             ......\
             ",
            top_left_of(res.grid)
        );
        assert_eq!(res.t, 2);
        assert_eq!(res.x, 0);
        assert_eq!(res.y, 3);
        assert_eq!(res.d, [-1, 0]);

        lst.handle_game_tick();

        // Repeat the command to player 1, to see the end state
        let res = lst.handle_move(req_1_left).unwrap();

        assert_eq!(
            "\
             ......\n\
             ..Bbb.\n\
             a.....\n\
             a.....\n\
             ......\n\
             ......\
             ",
            top_left_of(res.grid)
        );
        assert_eq!(res.t, 3);
        assert_eq!(res.x, 2);
        assert_eq!(res.y, 1);
        assert_eq!(res.d, [-1, 0]);
    }

    fn top_left_of(grid: Vec<String>) -> String {
        grid.iter()
            .take(6)
            .map(|ln| ln[..6].to_string())
            .collect::<Vec<_>>()
            .join("\n")
    }
}
