use actix::prelude::*;
use actix_web::Error;

pub struct GamesRequest {}

impl Message for GamesRequest {
    type Result = Result<GamesResponse, Error>;
}

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum GameStatus {
    NotStarted,
    StartingIn(u128),
    Running,
    Finished(u128),
}

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct GamesResponseEntry {
    pub gameid: String,
    pub status: GameStatus,
}

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct GamesResponse {
    pub games: Vec<GamesResponseEntry>,
}
