use actix::prelude::*;
use actix_web::Error;

#[derive(Debug, Deserialize, Serialize)]
pub struct JoinRequest {
    pub name: String,
}

impl Message for JoinRequest {
    type Result = Result<JoinResponse, Error>;
}

#[derive(Debug, Deserialize, Serialize)]
pub struct JoinResponse {
    pub gameid: String,
    pub name: String,
    pub index: usize,
    pub secret: String,
}
