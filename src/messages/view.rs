use actix::prelude::*;
use actix_web::Error;

use crate::messages::games::GameStatus;

#[derive(Debug, Deserialize, Serialize)]
pub struct ViewRequest {
    pub gameid: String,
}

impl Message for ViewRequest {
    type Result = Result<ViewResponse, Error>;
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ViewResponse {
    pub gameid: String,
    pub status: GameStatus,
    pub t: u32,
    pub grid: Vec<String>,
    pub players: Vec<Player>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Player {
    pub name: String,
    pub pos: Position,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}
