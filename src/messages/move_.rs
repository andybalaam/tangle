use actix::prelude::*;
use actix_web::Error;

#[derive(Clone)]
pub struct MoveRequest {
    pub gameid: String,
    pub player_name: String,
    pub player_secret: String,
    pub new_dir: [i8; 2],
}

impl Message for MoveRequest {
    type Result = Result<MoveResponse, Error>;
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct MoveResponse {
    pub gameid: String,
    pub t: u32,
    pub x: i32,
    pub y: i32,
    pub d: [i8; 2],
    pub grid: Vec<String>,
}
