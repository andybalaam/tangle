extern crate tangle;

use actix::prelude::*;
use actix_web::test::TestServer;
use actix_web::{http, HttpMessage};
use serde::de::DeserializeOwned;
use serde::Serialize;

use tangle::app;
use tangle::game_list::GameList;
use tangle::messages::games::GameStatus;
use tangle::messages::games::GamesResponse;
use tangle::messages::join::JoinRequest;
use tangle::messages::join::JoinResponse;

struct Setup {
    server: TestServer,
}

impl Setup {
    fn new() -> Setup {
        let _system = System::new("test");
        let game_list = GameList::new();
        let game_list_addr = Arbiter::start(|_ctx| game_list);

        let server = TestServer::with_factory(move || {
            app::create(game_list_addr.clone())
        });
        Setup { server }
    }

    fn get<S: DeserializeOwned + 'static>(&mut self, path: &str) -> S {
        let request = self
            .server
            .client(http::Method::GET, path)
            .finish()
            .unwrap();

        let response = self.server.execute(request.send()).unwrap();
        assert!(response.status().is_success());

        self.server.execute(response.json()).unwrap()
    }

    fn post<S: DeserializeOwned + 'static, T: Serialize>(
        &mut self,
        path: &str,
        req: T,
    ) -> S {
        let request = self
            .server
            .client(http::Method::POST, path)
            .json(req)
            .unwrap();

        let response = self.server.execute(request.send()).unwrap();
        assert!(response.status().is_success());

        self.server.execute(response.json()).unwrap()
    }
}

#[test]
fn join_creates_a_game() {
    let mut setup = Setup::new();

    // Initially, there are no games
    let games_list: GamesResponse = setup.get("/games");
    assert_eq!(games_list.games.len(), 0);

    // Ask to join and get back a game ID
    let response: JoinResponse = setup.post(
        "/join",
        JoinRequest {
            name: String::from("a"),
        },
    );
    assert!(response.gameid.len() > 0);

    // Now a game exists, and its ID is the returned game id
    let games_list: GamesResponse = setup.get("/games");
    assert_eq!(games_list.games.len(), 1);
    assert_eq!(games_list.games[0].gameid, response.gameid);
    assert_eq!(games_list.games[0].status, GameStatus::NotStarted);
}

/* TODO: work out mock Now
tokio::tokio_timer::clock::with_default(
*/

#[test]
fn when_2_people_join_the_game_becomes_starting() {
    let mut setup = Setup::new();

    // Given a not-started game
    let _response: JoinResponse = setup.post(
        "/join",
        JoinRequest {
            name: String::from("a"),
        },
    );
    let games_list: GamesResponse = setup.get("/games");
    assert_eq!(games_list.games[0].status, GameStatus::NotStarted);

    // When another person joins
    let response: JoinResponse = setup.post(
        "/join",
        JoinRequest {
            name: String::from("b"),
        },
    );

    // Then the game starts
    let games_list: GamesResponse = setup.get("/games");
    assert_eq!(games_list.games[0].gameid, response.gameid);
    match &games_list.games[0].status {
        GameStatus::StartingIn(_) => (),
        other => panic!("Unexpected game status: {:?}", other),
    }
}
